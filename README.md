Serialized Singletons
============
A base class for serializing singletons in Unity3D.

Singletons are a great pattern in game development. Like anything, it can be abused, but it's also a pattern that doesn't align well to the Unity methodology. This script will automatically generate a file containing instance fields for your singleton, and on Awake load them into static variables. For the end-user (end-developer?) this means that static variables on singleton objects can be serialized.

Besides meeting the below constraint, this extension is designed to just *work*. When you add a "public static float MyFloat;", this extension automatically compiles in the instance fields for unity to serialize. It even works with Unity objects - no setup required!

There are three requirements for serializing a singleton:

- Class must derive from SerializedSingleton<MyClassType>
- Class must be partial
- Code must be written in C#

Here is an example of a serialized singleton. Say you want to define some global colors which are used throughout your code. You want to be able to use Unity's color picker to define these colors, and have them statically accessible. Here is the singleton class:
```
public partial class Colors : SerializedSingleton<Colors> 
{

	public static Color health;
	public static Color[] teams;
	
}
```
Without the Serialized Singleton class, you would never be able to modify these values in an inspector and they'd never be serialized. However, the importer automatically generates the other half of this class:
```
public partial class Colors
{

	[SerializeField]
	Color _health;
	
	[SerializeField]
	Color[] _teams;
	
}
```
When the object awakes, these private, serialized instance variables are copied into their respective static fields. Then, accessing colors is this simple:
```
renderer.material.color = Colors.health;
```

That should be all there is to it - get coupling, cowboys.
